#include <stdio.h>
#include <stdlib.h>

#include "pt_utils.h"

int pt_rand(int nbits) {
    int mask;
    if (0 < nbits && nbits < sizeof(int)*8) {
        // the least significant nbits bits will be 1, others will be 0
        mask = ~(~((unsigned int) 0) << nbits); 
    }
    else {
        // the mask will be all ones
        mask = ~((unsigned int) 0);
    }
    // get the next random number and keep only nbits bits
    return rand() & mask;
}

int * gen_rand_int_array(int length, int nbits) {

    int * a = malloc(sizeof(int)*length);
    for (int i = 0; i < length; i++) {
        a[i] = pt_rand(nbits);
    }
    return a;
}

void copy_int_array(int src[], int dst[], int length) {

    for (int i = 0; i < length; i++) {
        dst[i] = src[i];
    }
}

int * clone_int_array(int a[], int length) {

    int * clone = malloc(sizeof(int)*length);
    copy_int_array(a, clone, length);
    
    return clone;
}

void print_int_array(int a[], int length) {

    for (int i = 0; i < length; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void swap_pointers(int* x, int* y) {
    int tmp = *x;
//    printf("*x: %i; *y: %i\n", *x, *y);
    *x = *y;
    *y = tmp;
//    printf("*x: %i; *y: %i\n", *x, *y);
}

void shuffle_int_array(int a[], int length) {
    // select a random integer from a[]
    for (int i = length - 1; i > 0; i--){   // stops at 0, since there'd only be 1 number left
        int r = rand() % i; // should have different ranges every loop, but NOT, somehow
        printf("%i - Random index: %i - Value: %i\n", i, r, a[r]);
        swap_pointers(&a[r], &a[i]);
        print_int_array(a, length);
    }
}

//TODO: REMOVE THIS IF NOT NEEDED LATER
void selection_sort(int a[], int length) {
    int min_pos;
    // Borrowed from pt_sorting
    /* find the min value in a[i] to a[length-1], and swap it with a[i] */
    for (int i = 0; i < length; i++) {
        min_pos = i;
        /* find the minimum value in what is left of the array */
        for (int j = i+1; j < length; j++) {
            if (a[j] < a[min_pos]) {
                min_pos = j;
            }
        }
        swap_pointers(&a[i], &a[min_pos]);
    }
}

int linear_search(int a[], int length, int target){
    // apply linear search to find an integer
    // should work whether the array is sorted or not
    int default_index = -1;
    for (int i = 0; i < length; i++){
        if (a[i] == target){
//            index = i;
            printf("Found number %i at: %i\n", target, i);
            return i;
        }
    }
    printf("Could not find number %i, code: %i\n", target, default_index);
    return default_index;
}

int binary_search(int a[], int length, int target){
    // apply binary search to find target integer
    // the array is REQUIRED to be sorted in ascending value
    int min_index = 0;
    int max_index = length - 1;

    for (int i = 0; i < length; i++){   // put a constraint on the loop, in case something goes wrong
        int mid_index = (max_index + min_index) / 2;

        printf("Searching for %i in index range: [%i - %i]\n", target, min_index, max_index);
        printf("Middle index: %i - Value: %i\n", mid_index, a[mid_index]);
        if (a[mid_index] == target) {    // found the target
            printf("Found number %i at index %i\n", target, mid_index);
            return mid_index;
        }
        else if (a[mid_index] > target){
            printf("Target is lower!\n");
            max_index = mid_index - 1;
        }
        else if (a[mid_index] < target){    // target is higher
            printf("Target is higher!\n");
            min_index = mid_index + 1;
        }

        if (min_index > max_index) {
            printf("Target outside range!\n");
            break;
        }
//        if (max_index - min_index <= 1) {
//            if (a[max_index] == target) {
//                printf("Found number %i at index:%i\n", target, max_index);
//                return max_index;
//            }
//            else if (a[min_index] == target) {
//                printf("Found number %i at index:%i\n", target, min_index);
//                return min_index;
//            }
//            else {
//                printf("Could not find %i, code: %i\n", target, default_index);
//                return default_index;
//            }
//        }

    }
    printf("Could not find %i, code: %i\n", target, -1);
    return -1;
}