#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "pt_utils.h"


int main(int argc, char * argv[]) {
        
    int length = 10;
    int nbits = 3;
    if (argc >= 2) {
        length = atoi(argv[1]);        
    }
    if (argc >= 3) {
        nbits = atoi(argv[2]);        
    }
    if (argc >= 4) {
        if (argv[3][0] == 't') {
            time_t current_time = time(NULL);
            srand(current_time);
            printf("time: %s\n", ctime(&current_time));
        }
        else {
            srand(atoi(argv[3]));
        }
    }
    
//    int * a = gen_rand_int_array(length, nbits);nbits
//    int * clone = clone_int_array(a, length);
   
/*    print_int_array(clone, length);*/
    time_t start_t = time(NULL);
    clock_t start_c = clock();
    time_t end_t = time(NULL);
    clock_t end_c = clock();
/*    print_int_array(clone, length);*/
    
    printf("Time: %f\n", difftime(end_t, start_t));
    printf("Clock (micros): %ld\n", end_c-start_c);
    printf("Clock (ms): %f\n", (end_c-start_c)/1000.0);
    printf("Clock (s): %f\n", (end_c-start_c)/1000000.0);
    
//    free(clone);
//    free(a);
}
