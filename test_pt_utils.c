#include <stdio.h>
#include <stdlib.h>

#include "pt_utils.h"


int main(int argc, char * argv[]) {
        
//    int length = 10000;
//    int nbits = 4;
//
//    int * a = gen_rand_int_array(length, nbits);
//    int * clone = clone_int_array(a, length);
//
//    //print_int_array(clone, length);
//    selection_sort(clone, length);
//    //print_int_array(clone, length);
//
//    free(clone);
//    free(a);

    int c;
    printf("Which test to run? ");
    int choice = scanf("%i", &c);
    if (choice > 0) {
        if (c == 0) {
            // swap_pointers test
            printf("   ===== START SWAP TEST =====   \n");

            int x, y;

            printf("Enter integer 1: ");
            int m = scanf("%i", &x);
            printf("Enter integer 2: ");
            int n = scanf("%i", &y);
            if (m == 0 || n == 0) {
                printf("Invalid Input!\n");
            } else {
                printf("Before swap - x: %i, y: %i\n", x, y);
                swap_pointers(&x, &y);
                printf("After swap - x: %i, y: %i\n", x, y);
            }
            printf("     ===== END SWAP TEST =====     \n\n");
        } else if (c == 1) {
            // shuffle_int_array test
            printf("    ===== START SHUFFLE TEST =====    \n");
            int len,bitlen;
            printf("Enter desired array length: ");
            int m = scanf("%i", &len);
            printf("Enter desired bit length: ");
            int n = scanf("%i", &bitlen);
            if (m == 0 || n == 0) {
                printf("Invalid Input!\n");
            } else {
                int *rand_arr_shu = gen_rand_int_array(len, bitlen);
                printf("Int Array before shuffle: ");
                print_int_array(rand_arr_shu, len);
                shuffle_int_array(rand_arr_shu, len);
                printf("Int Array after shuffle: ");
                print_int_array(rand_arr_shu, len);
                free(rand_arr_shu);
            }
            printf("     ===== END SHUFFLE TEST =====     \n\n");
        } else if (c == 2) {
            // linear_search test
            printf("    ===== START LINEAR TEST =====    \n");
            int len, bitlen, target;
            printf("Enter desired array length: ");
            int m = scanf("%i", &len);
            printf("Enter desired bit length: ");
            int n = scanf("%i", &bitlen);
            if (m == 0 || n == 0) {
                printf("Invalid Input!\n");
            } else {
                int *rand_arr_lin = gen_rand_int_array(len, bitlen);
                printf("Int Array: ");
                print_int_array(rand_arr_lin, len);

                printf("Enter search target: ");
                int o = scanf("%i", &target);
                if (o == 0) {
                    printf("Invalid Input!\n");
                } else {
                    linear_search(rand_arr_lin, len, target);
                }
                free(rand_arr_lin);
            }
            printf("     ===== END LINEAR TEST =====     \n\n");
        } else if (c == 3) {
            // binary_search_test
            printf("    ===== START BINARY TEST =====    \n");
            int len, bitlen, target;
            printf("Enter desired array length: ");
            int m = scanf("%i", &len);
            printf("Enter desired bit length: ");
            int n = scanf("%i", &bitlen);
            if (m == 0 || n == 0) {
                printf("Invalid Input!\n");
            } else {
                int *rand_arr_bin = gen_rand_int_array(len, bitlen);
                selection_sort(rand_arr_bin, len);
                printf("Int Array: ");
                print_int_array(rand_arr_bin, len);

                printf("Enter search target: ");
                int o = scanf("%i", &target);
                if (o == 0) {
                    printf("Invalid Input!\n");
                } else {
                    binary_search(rand_arr_bin, len, target);
                }
                free(rand_arr_bin);
            }
            printf("     ===== END BINARY TEST =====     \n\n");
        }
    }

}
