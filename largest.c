#include <stdio.h>
#include <stdbool.h>

int main() {
    int x = 0;
    bool found_int = false;
    int largest = 0;
    while (true) {
        int n = scanf("%i", &x);

        if (n > 0) {
            // Scanned an int
            if (x > largest || found_int == false) {
                largest = x;
            }
            found_int = true;
//            printf("Input accepted: %i\n", x);
        } else {
            // Flush
            int c = getc(stdin);
            while (c != '\n' && c != EOF) {
                // Until the end of the line
                c = getc(stdin);
            }
            if (c == EOF) {
                if (found_int == false) {
                    printf("No valid input\n");
                }
                else {
                    printf("The largest number is: %i\n", largest);
                }
                break;
            }
        }
    }

    return 0;
}