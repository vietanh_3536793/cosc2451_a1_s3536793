#include <stdio.h>
#include <stdbool.h>

int main() {
    int x = 0;
    int linecount = 1;
    int counttotal = 0;
    int target = 0;
    bool found_int = false;
    // Get the first int
    while (true) {
        int n = scanf("%i", &x);

        if (n > 0) {
            // Scanned an int
            if (found_int == false) {
                // Assign the target integer as the first to be found
                target = x;
                found_int = true;
            }
            else if (x == target) {
                // Print every time target is found
                printf("Number %i was found at position: %i\n", target, linecount);
                counttotal += 1;
            }
            linecount += 1;
        } else {
            // Flush
            int c = getc(stdin);
            while (c != '\n' && c != EOF) {
                // Until the end of the line
                c = getc(stdin);
            }
            if (c == EOF) {
                if (found_int == false) {
                    printf("No integer was found!\n");
                } else {
                    printf("Number %i was found %i times\n", target, counttotal);
                }
                break;
            }
        }
    }
    return 0;
}